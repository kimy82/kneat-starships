# KNEAT STARSHIPS

## User Story

As a commander I want to know how many stops are required for each of the starships available in the API.

## HOW TO RUN

### USING DOCKER (BUILDING THE IMAGE)

From the root directory run the following commands.

#### First

```
docker build -t kimy82/kneat-starships .
```

#### Second

```
docker run -p 3000:3000 -d kimy82/kneat-starships
```

### USING DOCKER (PULLING FROM DOCKER HUB)

```
docker run -p 3000:3000 -d kimy82/kneat-starships
```

### WITHOUT DOCKER

### REQUIREMENTS
- NPM & NODE installed

```
npm start
```

## CI PIPELINE

The APP does have a very simple CI Pipeline. The first step runs *jest* tests and the second step deploys the APP to a Digital Ocean server.
