jest.mock('simple-node-logger', () => {
    return {
        createRollingFileLogger: () => {
            return { info: console.log, error: console.error }
        }
    }
});