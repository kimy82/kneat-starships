import '../../utils/mockLogger';
import API from '../../../src/modules/starships/API';
const mockAxiosInstance = {
    get: jest.fn((url: string) => {
        if (url === '/starships/?page=1') {
            return new Promise((resolve) => {
                resolve({ data: {
                    count: 20,
                    next: 'https://swapi.co/api/starships/?page=2',
                    previous: null,
                    results: [
                        {
                            name: 'Executor',
                            consumables: '6 years',
                            MGLT: '40'
                        },
                        {
                            name: 'Sentinel-class landing craft',
                            consumables: '1 month',
                            MGLT: '70'
                        },
                        {
                            name: 'Death Star',
                            consumables: '3 years',
                            MGLT: '10'
                        },
                        {
                            name: 'Millennium Falcon',
                            consumables: '2 months',
                            MGLT: '75'
                        },
                        {
                            name: 'Y-wing',
                            consumables: '1 week',
                            MGLT: '80'
                        },
                        {
                            name: 'X-wing',
                            consumables: '1 week',
                            MGLT: '100'
                        },
                        {
                            name: 'TIE Advanced x1',
                            consumables: '5 days',
                            MGLT: '105'
                        },
                        {
                            name: 'Slave 1',
                            consumables: '1 month',
                            MGLT: '70'
                        },
                        {
                            name: 'Imperial shuttle',
                            consumables: '2 months',
                            MGLT: '50'
                        },
                        {
                            name: 'EF76 Nebulon-B escort frigate',
                            consumables: '2 years',
                            MGLT: '40'
                        }
                    ]
                }});
            });
        } else if (url === '/starships/?page=2') {
            return new Promise((resolve) => {
                resolve({ data: {
                    count: 14,
                    next: null,
                    previous: 'https://swapi.co/api/starships/?page=1',
                    results: [
                        {
                            name: 'Calamari Cruiser',
                            consumables: '2 years',
                            MGLT: '60'
                        },
                        {
                            name: 'A-wing',
                            consumables: '1 week',
                            MGLT: '120'
                        },
                        {
                            name: 'B-wing',
                            consumables: '1 week',
                            MGLT: '91'
                        },
                        {
                            name: 'Republic Cruiser',
                            consumables: 'unknown',
                            MGLT: 'unknown'
                        },
                        {
                            name: 'Naboo fighter',
                            consumables: '7 days',
                            MGLT: 'unknown'
                        },
                        {
                            name: 'Naboo Royal Starship',
                            consumables: 'unknown',
                            MGLT: 'unknown'
                        },
                        {
                            name: 'Scimitar',
                            consumables: '30 days',
                            MGLT: 'unknown'
                        },
                        {
                            name: 'J-type diplomatic barge',
                            consumables: '1 year',
                            MGLT: 'unknown'
                        },
                        {
                            name: 'AA-9 Coruscant freighter',
                            consumables: 'unknown',
                            MGLT: 'unknown'
                        },
                        {
                            name: 'Jedi starfighter',
                            consumables: '7 days',
                            MGLT: 'unknown'
                        }
                    ]
                }});
            });
        }
    })
};

jest.mock('axios', () => {
    return {
        default: {
            create: () => {
                return mockAxiosInstance;
            }
        }
    };
});

test('should retireve all starships', async () => {
    const starshipsAPI = new API();
    const allStarships = await starshipsAPI.getAllStarships();
    expect(allStarships.length).toBe(20);
    expect(mockAxiosInstance.get).toHaveBeenCalledTimes(2);
});
