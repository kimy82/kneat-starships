import '../utils/mockLogger';
import StarshipsRoutes from '../../src/routes/StarshipsRoutes';
import StarshipsService from '../../src/services/StarshipsService';

jest.mock('../../src/services/StarshipsService');

const mockGetStopsForMglt = jest.fn((mglt) => {
    return new Promise<IStops[]>((resolve) => {
        if(mglt === 100) {
            resolve([{ name: 'teststarship', stops: 3 }]);
        }
    });
});

(StarshipsService as any).mockImplementation(() => { return {
    getStopsForMglt: mockGetStopsForMglt
}; });

beforeEach( () => {
    mockGetStopsForMglt.mockClear();
});

test('should retrieve starship stops when mglt is in the request', async () => {
    const starshipsRoutes = new StarshipsRoutes();
    const req = { query: { mglt: 100 }};
    const res = { render: jest.fn() };
    await (starshipsRoutes as any).showIndex(req, res);

    expect(mockGetStopsForMglt).toHaveBeenCalledTimes(1);
    expect(res.render).toHaveBeenCalledWith('index', { stops : [{ name: 'teststarship', stops: 3 }], mglt: 100 });
});

test('should not retrieve starship stops when mglt is not in the request', async () => {
    const starshipsRoutes = new StarshipsRoutes();
    const req = { query: { } };
    const res = { render: jest.fn() };
    await (starshipsRoutes as any).showIndex(req, res);

    expect(mockGetStopsForMglt).not.toHaveBeenCalled();
    expect(res.render).toHaveBeenCalledWith('index', { });
});
