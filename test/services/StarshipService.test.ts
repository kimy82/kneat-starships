import '../utils/mockLogger';
import API from '../../src/modules/starships/API';
import StarshipsService from '../../src/services/StarshipsService';

jest.mock('../../src/modules/starships/API');

let starShips: IStarships[] = [];
let starshipsService: StarshipsService;

const mockGetAllStarShips = jest.fn(() => {
    return new Promise<IStarships[]>((resolve) => {
        resolve(starShips);
    });
});

(API as any).mockImplementation(() => {
    return {
        getAllStarships: mockGetAllStarShips
    };
});

beforeEach(() => {
    starshipsService = new StarshipsService();
    mockGetAllStarShips.mockClear();
});

test('should retrieve starship stops when mglt is 1000000', async () => {
    starShips = [
        { name: 'Y-Wing', consumables: '1 week', MGLT: 80 },
        { name: 'Millenium Falcon', consumables: '2 months', MGLT: 75 },
        { name: 'Rebel Transport', consumables: '6 months', MGLT: 20 },
        { name: 'Millenium Bird', consumables: '2 years', MGLT: 90 }
    ];

    const starShipsStops = await starshipsService.getStopsForMglt(1000000);

    expect(starShipsStops.find((s) => s.name === 'Y-Wing').stops).toBe(74);
    expect(starShipsStops.find((s) => s.name === 'Millenium Falcon').stops).toBe(9);
    expect(starShipsStops.find((s) => s.name === 'Rebel Transport').stops).toBe(11);
    expect(starShipsStops.find((s) => s.name === 'Millenium Bird').stops).toBe(1);
});

test('should retrieve and empty array of starship stops when no ships are returned by the API', async () => {
    starShips = [ ];
    const starShipsStops = await starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.length).toBe(0);
});

test('should set stops as unkown when consumables is unkown', async () => {
    starShips = [
        { name: 'Y-Wing', consumables: 'unknown', MGLT: 80 }
    ];
    const starShipsStops = await starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.find((s) => s.name === 'Y-Wing').stops).toBe('unknown');
});

test('should set stops as unkown when MGLT is unkown', async () => {
    starShips = [
        { name: 'Y-Wing', consumables: '2 days', MGLT: 'unknown' }
    ];
    const starShipsStops = await starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.find((s) => s.name === 'Y-Wing').stops).toBe('unknown');
});
