FROM node:8

#WORKDIRECTORY
RUN mkdir -p /usr/src/kneat-starships
WORKDIR /usr/src/kneat-starships

#DEPENDENCIES
COPY . /usr/src/kneat-starships/
RUN npm install
RUN npm i -g typescript
RUN tsc -p .

EXPOSE 3000
CMD [ "node", "dist/index.js" ]