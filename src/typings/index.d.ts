interface IStops {
    name: string;
    stops: number | 'unknown';
}

interface IStarships {
    name: string;
    MGLT: number | 'unknown';
    consumables: string;
}

interface IStarshipsPage {
    next: string;
    results : IStarships[];
}