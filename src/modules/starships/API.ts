import { retryWhen, delay, take } from 'rxjs/operators';
import * as Rx from 'rxjs';
import axios, { AxiosInstance } from 'axios';
import * as querystring from 'query-string';
import Config from '../../Config';

export default class API {

    private axiosInstance: AxiosInstance;

    constructor() {
        this.axiosInstance = axios.create({ baseURL: Config.baseUrl, timeout: Config.requestTimeout, headers: { }, responseType: 'text' });
    }

    public getAllStarships = async (): Promise<IStarships[]> => {
        let nextPage = 1;
        let starships: IStarships[] = [];
        while(nextPage) {
            const starshipsPage = await this.getStarshipsPage(nextPage).toPromise();
            if(starshipsPage.next) {
                const queryParams = querystring.parseUrl(starshipsPage.next);
                nextPage = parseInt(queryParams.query.page as string, 10);
            } else {
                nextPage = null;
            }
            starships = starships.concat(starshipsPage.results);
        }
        return starships;
    }

    private getStarshipsPage = (page: number): Rx.Observable<IStarshipsPage> => {
        return new Rx.Observable<IStarshipsPage>( (observer) => {
            this.axiosInstance.get(`/starships/?page=${page}`).then( (response) => {
                if (response.data) {
                    observer.next(response.data);
                    observer.complete();
                } else {
                    observer.error('Error getting Starships');
                }
            })
                .catch( (error) => {
                    observer.error(error);
                });

        }).pipe(retryWhen( (err) => err.pipe(delay(1000), take(5))));
    }
}
