import * as dotenv from 'dotenv';

dotenv.config();

export default class Config {
    public static baseUrl: string = process.env.API_URL;
    public static rootDir: string = process.cwd();
    public static requestTimeout: number = parseInt(process.env.TIMEOUT, 10);
}
