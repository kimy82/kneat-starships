import API from '../modules/starships/API';
import LoggerService from './LoggerService';

export default class StarshipsService {

    private static UNKNOWN: 'unknown' = 'unknown';
    private starShipsAPI: API;

    constructor() {
        this.starShipsAPI = new API();
    }

    public getStopsForMglt = async (mglt: number): Promise<IStops[]> => {
        const starships = await this.starShipsAPI.getAllStarships();
        const stops: IStops[] = [];
        for(const starship of starships) {
            LoggerService.logger.info(`Starship name ->  ${starship.name}`);
            if(starship.MGLT !== StarshipsService.UNKNOWN) {
                try {
                    const stopsNumber = (mglt / starship.MGLT) / this.getHoursFromConsumables(starship.consumables);
                    stops.push({ name: starship.name, stops: Math.round(stopsNumber) });
                } catch (e) {
                    stops.push({ name: starship.name, stops: StarshipsService.UNKNOWN });
                }
            } else {
                stops.push({ name: starship.name, stops:  StarshipsService.UNKNOWN });
            }
        }
        return stops;

    }

    private getHoursFromConsumables = (consumables: string): number => {
        const consumablesTrimed = consumables.trim().toLowerCase();
        const value = parseInt(consumablesTrimed.split(' ')[0], 10);
        let hours = 0;
        if(consumablesTrimed.endsWith('week')) {
            hours = value * 7 * 24;
        } else if(consumablesTrimed.endsWith('day') || consumablesTrimed.endsWith('days')) {
            hours = value * 24;
        } else if(consumablesTrimed.endsWith('month') || consumablesTrimed.endsWith('months')) {
            hours = value * 31 * 24;
        }  else if(consumablesTrimed.endsWith('year') || consumablesTrimed.endsWith('years')) {
            hours = value * 256 * 24;
        } else {
            throw new Error(`Mesure of time not detected ${consumablesTrimed}`);
        }
        return hours;
    }
}
