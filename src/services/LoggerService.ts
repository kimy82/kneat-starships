import * as SimpleNodeLogger from 'simple-node-logger';
import Config from '../Config';
export default class LoggerService {
    public static logger = SimpleNodeLogger.createRollingFileLogger( {
        timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS',
        logDirectory: Config.rootDir + '/logs',
        fileNamePattern: 'kneat-starships-<DATE>.log',
        dateFormat: 'YYYY.MM.DD'
    });
}
