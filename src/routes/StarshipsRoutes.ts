import * as express from 'express';
import LoggerService from '../services/LoggerService';
import StarshipsService from '../services/StarshipsService';
import { Request, Response } from 'express-serve-static-core';

const router = express.Router();

export default class StarshipsRoutes {

    private starshipsService: StarshipsService;

    constructor() {
        this.starshipsService = new StarshipsService();
    }

    public buildRoutes(): express.Router {
        router.get('', this.showIndex);
        return router;
    }

    private showIndex = async (req: Request, res: Response) => {
        try {
            if(req.query && req.query.mglt) {
                const mglt = req.query.mglt;
                LoggerService.logger.info(`MGLT from query params -> ${mglt}`);
                const stops = await this.starshipsService.getStopsForMglt(mglt);
                res.render('index', { stops : stops, mglt: mglt });
            } else {
                res.render('index', { });
            }
        } catch(e) {
            LoggerService.logger.error('Error in showIndex', e);
            res.status(500).json({ error: 'Error in showIndex' });
        }
    }
}
