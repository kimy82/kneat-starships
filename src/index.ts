import * as express from 'express';
import * as session from 'express-session';
import * as path from 'path';
import Config from './Config';
import LoggerService from './services/LoggerService';
import StarshipsRoutes from './routes/StarshipsRoutes';


const app = express();

app.set('view engine', 'pug');
app.set('views', path.join(Config.rootDir, 'src/views'));
app.use('/static', express.static('src/assets'));

app.get('/health-check', (req, res) => { res.send('ok'); });
app.use('/starships', new StarshipsRoutes().buildRoutes());

app.listen(3000, () => { LoggerService.logger.info('Listening on port 3000!'); });
