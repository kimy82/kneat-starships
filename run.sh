echo "Building Docker Image"
if [ "$(docker ps -q -a -f name=kneat-starships)" ]; then
    docker rm -f kneat-starships
fi
docker build -t kimy82/kneat-starships .
docker run --restart unless-stopped --name kneat-starships -p 3000:3000 -d kimy82/kneat-starships
exit $?