"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
class Config {
}
Config.baseUrl = process.env.API_URL;
Config.rootDir = process.cwd();
exports.default = Config;
