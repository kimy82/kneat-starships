"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const operators_1 = require("rxjs/operators");
const Rx = require("rxjs");
const axios_1 = require("axios");
const querystring = require("query-string");
const Config_1 = require("../../Config");
class API {
    constructor() {
        this.getAllStarships = () => __awaiter(this, void 0, void 0, function* () {
            let nextPage = 1;
            let starships = [];
            while (nextPage) {
                const starshipsPage = yield this.getStarshipsPage(nextPage).toPromise();
                if (starshipsPage.next) {
                    const queryParams = querystring.parseUrl(starshipsPage.next);
                    nextPage = parseInt(queryParams.query.page, 10);
                }
                else {
                    nextPage = null;
                }
                starships = starships.concat(starshipsPage.results);
            }
            return starships;
        });
        this.getStarshipsPage = (page) => {
            return new Rx.Observable((observer) => {
                this.axiosInstance.get(`/starships/?page=${page}`).then((response) => {
                    if (response.data) {
                        observer.next(response.data);
                        observer.complete();
                    }
                    else {
                        observer.error('Error getting Starships');
                    }
                })
                    .catch((error) => {
                    observer.error(error);
                });
            }).pipe(operators_1.retryWhen((err) => err.pipe(operators_1.delay(1000), operators_1.take(5))));
        };
        this.axiosInstance = axios_1.default.create({ baseURL: Config_1.default.baseUrl, timeout: Config_1.default.requestTimeout, headers: {}, responseType: 'text' });
    }
}
exports.default = API;
