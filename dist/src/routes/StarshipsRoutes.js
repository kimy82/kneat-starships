"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const LoggerService_1 = require("../services/LoggerService");
const StarshipsService_1 = require("../services/StarshipsService");
const router = express.Router();
class StarshipsRoutes {
    constructor() {
        this.showIndex = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.query && req.query.mglt) {
                    const mglt = req.query.mglt;
                    LoggerService_1.default.logger.info(`MGLT from query params -> ${mglt}`);
                    const stops = yield this.starshipsService.getStopsForMglt(mglt);
                    res.render('index', { stops: stops, mglt: mglt });
                }
                else {
                    res.render('index', {});
                }
            }
            catch (e) {
                LoggerService_1.default.logger.error('Error in showIndex', e);
                res.status(500).json({ error: 'Error in showIndex' });
            }
        });
        this.starshipsService = new StarshipsService_1.default();
    }
    buildRoutes() {
        router.get('', this.showIndex);
        return router;
    }
}
exports.default = StarshipsRoutes;
