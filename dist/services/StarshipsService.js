"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const API_1 = require("../modules/starships/API");
const LoggerService_1 = require("./LoggerService");
class StarshipsService {
    constructor() {
        this.getStopsForMglt = (mglt) => __awaiter(this, void 0, void 0, function* () {
            const starShipsAPI = new API_1.default();
            const starships = yield starShipsAPI.getAllStarships();
            const stops = [];
            for (const starship of starships) {
                LoggerService_1.default.logger.info(`Starship name ->  ${starship.name}`);
                if (starship.MGLT !== 'unknown') {
                    const stopsNumber = (mglt / starship.MGLT) / this.getHoursFromConsumables(starship.consumables);
                    stops.push({ name: starship.name, stops: Math.round(stopsNumber) });
                }
                else {
                    stops.push({ name: starship.name, stops: 'unknown' });
                }
            }
            return stops;
        });
        this.getHoursFromConsumables = (consumables) => {
            const consumablesTrimed = consumables.trim().toLowerCase();
            const value = parseInt(consumablesTrimed.split(" ")[0], 10);
            let hours = 0;
            if (consumablesTrimed.endsWith('week')) {
                hours = value * 7 * 24;
            }
            else if (consumablesTrimed.endsWith('day') || consumablesTrimed.endsWith('days')) {
                hours = value * 24;
            }
            else if (consumablesTrimed.endsWith('month') || consumablesTrimed.endsWith('months')) {
                hours = value * 30 * 24;
            }
            else if (consumablesTrimed.endsWith('year') || consumablesTrimed.endsWith('years')) {
                hours = value * 256 * 24;
            }
            else {
                throw new Error(`Mesure of time not detected ${consumablesTrimed}`);
            }
            return hours;
        };
    }
}
exports.default = StarshipsService;
