"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SimpleNodeLogger = require("simple-node-logger");
const Config_1 = require("../Config");
class LoggerService {
}
LoggerService.logger = SimpleNodeLogger.createRollingFileLogger({
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS',
    logDirectory: Config_1.default.rootDir + '/logs',
    fileNamePattern: 'kneat-starships-<DATE>.log',
    dateFormat: 'YYYY.MM.DD'
});
exports.default = LoggerService;
