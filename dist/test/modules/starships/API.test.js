"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("../../utils/mockLogger");
const API_1 = require("../../../src/modules/starships/API");
const mockAxiosInstance = {
    get: jest.fn((url) => {
        if (url === '/starships/?page=1') {
            return new Promise((resolve) => {
                resolve({ data: {
                        count: 20,
                        next: 'https://swapi.co/api/starships/?page=2',
                        previous: null,
                        results: [
                            {
                                name: 'Executor',
                                consumables: '6 years',
                                MGLT: '40'
                            },
                            {
                                name: 'Sentinel-class landing craft',
                                consumables: '1 month',
                                MGLT: '70'
                            },
                            {
                                name: 'Death Star',
                                consumables: '3 years',
                                MGLT: '10'
                            },
                            {
                                name: 'Millennium Falcon',
                                consumables: '2 months',
                                MGLT: '75'
                            },
                            {
                                name: 'Y-wing',
                                consumables: '1 week',
                                MGLT: '80'
                            },
                            {
                                name: 'X-wing',
                                consumables: '1 week',
                                MGLT: '100'
                            },
                            {
                                name: 'TIE Advanced x1',
                                consumables: '5 days',
                                MGLT: '105'
                            },
                            {
                                name: 'Slave 1',
                                consumables: '1 month',
                                MGLT: '70'
                            },
                            {
                                name: 'Imperial shuttle',
                                consumables: '2 months',
                                MGLT: '50'
                            },
                            {
                                name: 'EF76 Nebulon-B escort frigate',
                                consumables: '2 years',
                                MGLT: '40'
                            }
                        ]
                    } });
            });
        }
        else if (url === '/starships/?page=2') {
            return new Promise((resolve) => {
                resolve({ data: {
                        count: 14,
                        next: null,
                        previous: 'https://swapi.co/api/starships/?page=1',
                        results: [
                            {
                                name: 'Calamari Cruiser',
                                consumables: '2 years',
                                MGLT: '60'
                            },
                            {
                                name: 'A-wing',
                                consumables: '1 week',
                                MGLT: '120'
                            },
                            {
                                name: 'B-wing',
                                consumables: '1 week',
                                MGLT: '91'
                            },
                            {
                                name: 'Republic Cruiser',
                                consumables: 'unknown',
                                MGLT: 'unknown'
                            },
                            {
                                name: 'Naboo fighter',
                                consumables: '7 days',
                                MGLT: 'unknown'
                            },
                            {
                                name: 'Naboo Royal Starship',
                                consumables: 'unknown',
                                MGLT: 'unknown'
                            },
                            {
                                name: 'Scimitar',
                                consumables: '30 days',
                                MGLT: 'unknown'
                            },
                            {
                                name: 'J-type diplomatic barge',
                                consumables: '1 year',
                                MGLT: 'unknown'
                            },
                            {
                                name: 'AA-9 Coruscant freighter',
                                consumables: 'unknown',
                                MGLT: 'unknown'
                            },
                            {
                                name: 'Jedi starfighter',
                                consumables: '7 days',
                                MGLT: 'unknown'
                            }
                        ]
                    } });
            });
        }
    })
};
jest.mock('axios', () => {
    return {
        default: {
            create: () => {
                return mockAxiosInstance;
            }
        }
    };
});
test('should retireve all starships', () => __awaiter(this, void 0, void 0, function* () {
    const starshipsAPI = new API_1.default();
    const allStarships = yield starshipsAPI.getAllStarships();
    expect(allStarships.length).toBe(20);
    expect(mockAxiosInstance.get).toHaveBeenCalledTimes(2);
}));
