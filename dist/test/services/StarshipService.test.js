"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("../utils/mockLogger");
const API_1 = require("../../src/modules/starships/API");
const StarshipsService_1 = require("../../src/services/StarshipsService");
jest.mock('../../src/modules/starships/API');
let starShips = [];
let starshipsService;
const mockGetAllStarShips = jest.fn(() => {
    return new Promise((resolve) => {
        resolve(starShips);
    });
});
API_1.default.mockImplementation(() => {
    return {
        getAllStarships: mockGetAllStarShips
    };
});
beforeEach(() => {
    starshipsService = new StarshipsService_1.default();
    mockGetAllStarShips.mockClear();
});
test('should retrieve starship stops when mglt is 1000000', () => __awaiter(this, void 0, void 0, function* () {
    starShips = [
        { name: 'Y-Wing', consumables: '1 week', MGLT: 80 },
        { name: 'Millenium Falcon', consumables: '2 months', MGLT: 75 },
        { name: 'Rebel Transport', consumables: '6 months', MGLT: 20 },
        { name: 'Millenium Bird', consumables: '2 years', MGLT: 90 }
    ];
    const starShipsStops = yield starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.find((s) => s.name === 'Y-Wing').stops).toBe(74);
    expect(starShipsStops.find((s) => s.name === 'Millenium Falcon').stops).toBe(9);
    expect(starShipsStops.find((s) => s.name === 'Rebel Transport').stops).toBe(11);
    expect(starShipsStops.find((s) => s.name === 'Millenium Bird').stops).toBe(1);
}));
test('should retrieve and empty array of starship stops when no ships are returned by the API', () => __awaiter(this, void 0, void 0, function* () {
    starShips = [];
    const starShipsStops = yield starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.length).toBe(0);
}));
test('should set stops as unkown when consumables is unkown', () => __awaiter(this, void 0, void 0, function* () {
    starShips = [
        { name: 'Y-Wing', consumables: 'unknown', MGLT: 80 }
    ];
    const starShipsStops = yield starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.find((s) => s.name === 'Y-Wing').stops).toBe('unknown');
}));
test('should set stops as unkown when MGLT is unkown', () => __awaiter(this, void 0, void 0, function* () {
    starShips = [
        { name: 'Y-Wing', consumables: '2 days', MGLT: 'unknown' }
    ];
    const starShipsStops = yield starshipsService.getStopsForMglt(1000000);
    expect(starShipsStops.find((s) => s.name === 'Y-Wing').stops).toBe('unknown');
}));
