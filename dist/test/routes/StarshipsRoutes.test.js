"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("../utils/mockLogger");
const StarshipsRoutes_1 = require("../../src/routes/StarshipsRoutes");
const StarshipsService_1 = require("../../src/services/StarshipsService");
jest.mock('../../src/services/StarshipsService');
const mockGetStopsForMglt = jest.fn((mglt) => {
    return new Promise((resolve) => {
        if (mglt === 100) {
            resolve([{ name: 'teststarship', stops: 3 }]);
        }
    });
});
StarshipsService_1.default.mockImplementation(() => {
    return {
        getStopsForMglt: mockGetStopsForMglt
    };
});
beforeEach(() => {
    mockGetStopsForMglt.mockClear();
});
test('should retrieve starship stops when mglt is in the request', () => __awaiter(this, void 0, void 0, function* () {
    const starshipsRoutes = new StarshipsRoutes_1.default();
    const req = { query: { mglt: 100 } };
    const res = { render: jest.fn() };
    yield starshipsRoutes.showIndex(req, res);
    expect(mockGetStopsForMglt).toHaveBeenCalledTimes(1);
    expect(res.render).toHaveBeenCalledWith('index', { stops: [{ name: 'teststarship', stops: 3 }], mglt: 100 });
}));
test('should not retrieve starship stops when mglt is not in the request', () => __awaiter(this, void 0, void 0, function* () {
    const starshipsRoutes = new StarshipsRoutes_1.default();
    const req = { query: {} };
    const res = { render: jest.fn() };
    yield starshipsRoutes.showIndex(req, res);
    expect(mockGetStopsForMglt).not.toHaveBeenCalled();
    expect(res.render).toHaveBeenCalledWith('index', {});
}));
